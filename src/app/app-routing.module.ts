import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MainMapComponent } from './main-map/main-map.component';


const routes: Routes = [
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'main-map',
    component:MainMapComponent
  },
  {
    path:'',
    redirectTo:'login',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
